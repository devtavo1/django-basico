from django.db import models

class Alumno(models.Model):
    # Atributos Alumnos
    nombre_completo = models.CharField(max_length=100)
    correo_electronico = models.EmailField()
    telefono = models.CharField(max_length=13) #9965458741
    curp = models.CharField(max_length=18)
    foto = models.ImageField(upload_to='profile_pictures/', blank=True, null=True)
    

    #Datos de inscripción
    #curp = models.FieldFile()
    #acta_nacimiento = models.FieldFile()    
    #comprobante_estudios = models.FieldFile()
    #comprobante_pago = models.FieldFile()
    #comprobante_domicilio = models.FieldFile()
    
    def __str__(self):
        return self.nombre_completo