from django.shortcuts import render, redirect
from django.http import HttpResponse
import csv

from .models import Curso

from .forms import CursoForm, CursoModelForm

def create_curso(request): # Create
    form = CursoModelForm()
    
    if request.method == 'POST':
        form = CursoModelForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('curso_lista')

    return render(request, 'cursos/create.html', {'form':form})


def list_curso(request):
    cursos_list = Curso.objects.all() # select * from cursos_curso;

    context = {
        'cursos_list':cursos_list,
    }
    return render(request, 'cursos/list.html', context)


def update_curso(request, pk):
    curso = Curso.objects.get(id=pk)
    mensaje = ''
    form = CursoModelForm(instance=curso)
    
    if request.method == 'POST':
        
        form = CursoModelForm(request.POST, instance=curso)
        if form.is_valid():
            form.save()

        mensaje = 'Actualizado correctamente'
    
    context = {'curso':curso, 'mensaje':mensaje, 'form':form}

    return render(request,'cursos/update.html', context)

def delete_curso(request,pk):
    Curso.objects.get(pk=pk).delete()
    print('se borro el curso =(')
    # from django.shortcuts import redirect
    return redirect('/curso/lista')


def datos(request):
    from_date = request.GET.get('from', None)
    to_date = request.GET.get('to', None)
    cursos_list = Curso.objects.all()
    print(from_date)
    print(to_date)
    
    if from_date and to_date:
        cursos_list = cursos_list.filter(fecha__range=[from_date, to_date])

    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="somefilename.csv"'},
    )
    
    writer = csv.writer(response)
    writer.writerow(["Descripcion", "Fecha","Titulo"])
    for curso in cursos_list:
        
        writer.writerow( [curso.descripcion, curso.fecha, curso.titulo])
    

    return response