from django.db import models


class Curso(models.Model):
    descripcion = models.TextField()
    titulo = models.CharField(max_length=100)
    fecha = models.DateField()

    def __str__(self):
        return self.titulo