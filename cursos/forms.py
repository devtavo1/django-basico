from django import forms
from .models import Curso

class CursoForm(forms.Form):
    descripcion = forms.CharField()


class CursoModelForm(forms.ModelForm):
    class Meta:
        model = Curso
        fields = '__all__' # ['titulo']
        #exclude = ['titulo']