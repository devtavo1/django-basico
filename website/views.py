from django.shortcuts import render


# vista basada en funcion
def index(request):
    return render(request,'website/index.html')